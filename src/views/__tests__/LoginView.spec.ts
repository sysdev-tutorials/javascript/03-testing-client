import { describe, it, expect, vi } from "vitest";
import { mount } from "@vue/test-utils";
import LoginView from "../LoginView.vue";
import { loginRequest } from "../../utils/httputils";
import { isLoginSuccessful } from "../../utils/loginutils";

describe("LoginView test suite", () => {
  it("renders LoginView properly", () => {
    const wrapper = mount(LoginView);
    console.log(wrapper.html());
    expect(wrapper.text()).toContain("Please login!");
  });

  it("change loginStatus to Success and test", async () => {
    const wrapper = mount(LoginView);

    // get loginstatusLabel element
    const statusId = wrapper.find("#loginstatusLabel");

    // change loginStatus to Success and check that loginstatusLabel element is updated accordingly
    await wrapper.setData({ loginStatus: "Success" });
    expect(statusId.element.textContent).toBe("Success");
  });

  it("change loginStatus to Failed and test", async () => {
    const wrapper = mount(LoginView);

    // get loginstatusLabel element
    const statusId = wrapper.find("#loginstatusLabel");

    // change loginStatus to Failed and check that loginstatusLabel element is updated accordingly
    await wrapper.setData({ loginStatus: "Failed" });
    expect(statusId.element.textContent).toBe("Failed");
  });

  it("test that handleLoginClick method is called on signin button click", () => {
    const wrapper = mount(LoginView);
    const confirmSpy = vi.spyOn(wrapper.vm, "handleLoginClick");
    wrapper.find("button").trigger("click");
    expect(confirmSpy).toHaveBeenCalled();
  });

  it("test that loginRequest and isLoginSuccessful methods are called on handleLoginClick method call", async () => {
    vi.mock("../../utils/httputils.js");
    loginRequest = vi.fn().mockImplementation();

    vi.mock("../../utils/loginutils.js");
    isLoginSuccessful = vi.fn().mockImplementation(false);

    const wrapper = mount(LoginView);
    await wrapper.vm.handleLoginClick();

    expect(loginRequest).toHaveBeenCalledTimes(1);
    expect(isLoginSuccessful).toHaveBeenCalledTimes(1);
  });

  it("test that LoginView displays Login failed! when login is unsuccessful", async () => {
    vi.mock("../../utils/httputils.js");
    loginRequest = vi.fn().mockImplementation();

    vi.mock("../../utils/loginutils.js");
    isLoginSuccessful = vi.fn().mockImplementation(false);

    const wrapper = mount(LoginView);
    await wrapper.vm.handleLoginClick();

    const statusId = wrapper.find("#loginstatusLabel");
    expect(statusId.element.textContent).toBe("Login failed!");
  });
});
