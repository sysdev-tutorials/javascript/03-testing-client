import { describe, it, expect, vi } from "vitest";
import { loginRequest } from "../httputils.js";
import axios from "axios";

vi.mock("axios");

describe("httputils tests", () => {
  it("test that axios.get method is called while calling loginRequest method", async () => {
    const spy = vi.spyOn(axios, "post");
    await loginRequest({});
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
