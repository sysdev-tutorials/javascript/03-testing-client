import { describe, it, expect } from "vitest";
import { isLoginSuccessful } from "../loginutils.js";

describe("login tests", () => {
  it("test login successful", () => {
    const successfulResponse = {
      data: {
        status: "true",
      },
    };
    expect(isLoginSuccessful(successfulResponse)).toBe(true);
  });

  it("test login fail", () => {
    const response = {
      data: {
        status: "false",
      },
    };
    expect(isLoginSuccessful(response)).toBe(false);
  });
});
