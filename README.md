# 03-Testing client

Testing is integral and very important part of modern software developement! Note that testing should start early in the development process, and should gradually and iteratively developed and improved. Tests should run as automated as possible.  

In this session, we will focus on testing of frontend part.

Note: install and use node v20, and if you get errors related to NODE_OPTIONS, execute ```unset NODE_OPTIONS``` command.



## Testing methods
Depending on how testing methodology (how the tester will analyze the application/system/component/unit under consideration), one can categorise testing methods as

- ```White-box testing```: The white box testing examins internal coding, design, and structure of an application/system/component/unit. This is leveraged to improve design, usability, security, and so on.
- ```Black-box testing```: The tester or testing team analyzes the workings of an application/system/component/unit without first having an extensive understanding of its internal structure and design. It is related to behavioral testing and do not require in-depth technical knowledge to carry it out.

## Test types
Depending on how tests are written, these can be categorized into following

- ```Unit tests```: include testing of smaller and isolated/self-contained parts of a code, such as methods, properties, smaller UI elements and so on. The purpose of a unit test is to check correctness and business logic for a small part of the overall application. It is lowest testing level, and usually cheapest and fastest to implement. Note that unit testing is usually of ```white-box``` in nature.
- ```Component or system tests```: include testing of an individual component or a system as a whole, such as that a component/system starts or mounts, can be interacted with, behaves as expected and so on. Usually such tests are more complex compared to unit tests, require more time to write and therefore more expensive. Note that depending on context and requirements, component or system tests can be both ```white-box``` or ```black-box``` in nature.
- ```Integration or end-to-end tests```: include interation between multiple components or systems and often involve interactions betwen UI and API, end-user interactions, network calls, and use of running application with databases and so on. Note that integration or e2e tests are more often ```black-box``` in nature.

In the following, we will look in details into each of the different types of tests!

## Prerequisite
Before we proceed, let us prepare a project to work on. For that it is assumed that you have followed all the steps described in previous lecture, https://gitlab.com/sysdev-tutorials/javascript/02-async-call-production-and-parsing-of-json

If you directly jumped here and did not follow the previous lectures, checkout the project and install depencencies by running ```npm i``` from the root directory of the project.


## Unit testing
We will use ```vitest``` library for writing and running tests. Note that it was installed when we have first create the project.

Note that it is configured under the ```scripts``` section in package.json file, like this!

```
"test:unit": "vitest --environment jsdom --root src/",
```

That means we can run the following command from the project folder and inspect that result. This will execute all the unit tests available under src directory. Make sure that the command executes successfully!

```
npm run test:unit
```

If you inspect, ```HelloWorld.spec.ts``` file under ```src/components/__tests__``` folder, you will see that there is one test suite indicated by ```describe``` block and one test case indicated by ```it``` block. 

Note that using ```describe``` you can define a new suite in the current context, as a set of related tests and/or other nested suites. In that sense, a suite lets you organize your tests! Note also that each individual tests are indicated with ```test``` block, which is also aliased with ```it``` block.

### Create unit test for util classes
Create ```loginutils.spec.ts``` file with following contents under ```src/utils/__tests__``` folder

Note that the file contains one test suite and one test! The test tests the behavior correctness of the ```isLoginSuccessful``` method from ```loginutils.js``` file. For example given the login successful data, the method return true!

```
import { describe, it, expect } from "vitest";
import {isLoginSuccessful} from "../loginutils.js"

describe("login tests", () => {
  it("test login successful", () => {
        let successfulResponse = {
            "data": {
                "status": "true"
            }
        }
        expect(isLoginSuccessful(successfulResponse)).toBe(true);
    });
});
```

One can add another test under the same test suite, for example to test that the ```isLoginSuccessful``` method return false given the login failure data, as below.

```
  it("test login fail", () => {
        let response = {
            "data": {
                "status": "false"
            }
        }
        expect(isLoginSuccessful(response)).toBe(false);
    });
```

Note also that one can add many tests for a single function. Since ```isLoginSuccessfull``` method depends on the value of the input parameter, we can write tests such that it returns false for ```null``` and ```undefined``` as inputs respectively. This is how one can write several unit tests (both positive and negative tests) for a given function or method!

```
  it("test login fail on no response", () => {
        let response = undefined
        expect(isLoginSuccessful(response)).toBe(false);
    });

  it("test login fail on null response", () => {
        let response = null
        expect(isLoginSuccessful(response)).toBe(false);
    });
```

Lets also create a simple unit test (```httputils.spec.js```) for our another utility file, ```httputils.js```. It contains a single test case that tests ```axios.get``` method is called one when the ```loginRequest``` method is called. Not that vi have imported ```vi``` utility from vitest and used its spyOn feature. Read more on that here https://vitest.dev/api/#vi-spyon

```
import { describe, it, expect, vi } from "vitest";
import {loginRequest} from "../httputils.js"
import axios from 'axios';

vi.mock('axios')
describe("httputils tests", () => {
    it("test that axios.get method is called while calling loginRequest method", async () => {
       const spy = vi.spyOn(axios, 'get');
       await loginRequest({});
       expect(spy).toHaveBeenCalledTimes(1);
    });
});
```

### Test coverage
Before continuing to other types of testing, lets have a quick look on 'Test coverage'. The test coverage measures the amount of testing performed by a set of tests. In a production code, this is one of the very important parameter and usually test coverage should be closer to 100%!

So, how do we see test coverage in our project? For that we will use a library called ```coverage-c8``` and we will install it via following command

```
npm i -D @vitest/coverage-c8
```

After the library is installed, lets add a alias command in package.json file, like below

```
"test:coverage": "vitest run --coverage --environment jsdom",
```

Thats it! We can now run a command ```npm run test:coverage``` from the project root folder and you shoud see test coverage statistics in the terminal.


## Component or system testing
As mentioned before, component testing sits above unit testing and below integration testing. But note that there is a grey area between unit and component testing, and between component and integration testing!

In general, components are isolated or self-contained units! In the contexts of Vue components, component testing include asserting whether a component mounts or renders or updates properly based on inputted properties and/or emitted events. And asserting does not include private state of the component instance or private methods of the component!

For example the ```HelloWord.vue``` component defines a property called ```msg``` and that property is used when component is mounted and rendered. So, one can write a unit test for that! Actually such test is already generated under ```components/__tests__/HelloWorld.spec.ts``` when project is created for the first time. Note here that, ```mount``` function from test-util (https://test-utils.vuejs.org/guide/#what-is-vue-test-utils) is used to mount or instantiate/render the component which creates a wrapper to act and assert against the Component.

```
// test in HelloWorld.spec.ts file
describe("HelloWorld", () => {
  it("renders properly", () => {
    const wrapper = mount(HelloWorld, { props: { msg: "Hello Vitest" } });
    expect(wrapper.text()).toContain("Hello Vitest");
  });
});
```

Lets write similar test for LoginView component by creating ```src/views/__tests__/LoginView.spec.ts``` file with following contents. Note that we are printing (console.log) wrapper.html(). Inspect the output, it will print rendered DOM of LoginView component.

```
import { describe, it, expect } from "vitest";
import { mount } from "@vue/test-utils";
import LoginView from "../LoginView.vue";

describe("LoginView test suite", () => {
  it("renders LoginView properly", () => {
    const wrapper = mount(LoginView);
    console.log(wrapper.html());
    expect(wrapper.text()).toContain("Please login!");
  });
});
```

Check code coverage at this point and compare it with the previous coverage report!!

Now, lets up go a step further and write some tests that involves component data. The ```LoginView``` component has three data variables and one of them is ```loginStatus```. Note also that the ```loginStatus``` data variable is rendered in a label element with id ```loginstatusLabel```. In the following we will write two tests that updates components data variable and test that corresponding data is updated in a label element.

```
  it('change loginStatus to Success and test', async () => {
    const wrapper = mount(LoginView)

    // get loginstatusLabel element
    const statusId = wrapper.find('#loginstatusLabel');

    // change loginStatus to Success and check that loginstatusLabel element is updated accordingly
    await wrapper.setData({loginStatus: 'Success'});
    expect(statusId.element.textContent).toBe('Success');
  })

  it('change loginStatus to Failed and test', async () => {
    const wrapper = mount(LoginView)

    // get loginstatusLabel element
    const statusId = wrapper.find('#loginstatusLabel');

    // change loginStatus to Failed and check that loginstatusLabel element is updated accordingly
    await wrapper.setData({loginStatus: 'Failed'});
    expect(statusId.element.textContent).toBe('Failed');
  })
```

Now let us test some event handling. For example when a ```Sign in``` button is clicked, we want to test that the ```handleLoginClick``` method is called. This can be achieved like below. 
Note that we have imported ```vi```utility from vitest library and used spyOn feature. The spyOn feature is useful when one need to validate whether or not a specific function has been called (and possibly which arguments were passed)! Read more here https://vitest.dev/api/#vi-spyon and https://test-utils.vuejs.org/guide/essentials/event-handling.html#the-counter-component

```
import { describe, it, expect, vi } from "vitest";
...
...
  it('test that handleLoginClick method is called on signin button click', () => {
        const wrapper = mount(LoginView)
        const confirmSpy = vi.spyOn(wrapper.vm, 'handleLoginClick')
        wrapper.find('button').trigger('click')
        expect(confirmSpy).toHaveBeenCalled()
  })
```

The above unit test does not test what updates happen in the UI when the signin button is clicked. Usually when writing unit tests on functions or components, it is recommended to focus ONE level depth, for example
- Upon Signin button click, test that handleLoginClick method is called 
- Upon calling handleLoginClick method, test that the isLoginSuccessful method is called

For testing multiple depth level or interaction between components or system, another type of test, called e2e or integrtation test, is recommended. This will be discussed later!

For the time being, let´s continue some more unit tests related to the LoginView component.

Let us directly call handleLoginClick method (i.e without clicking on Signin button) and then test that both loginRequest and isLoginSuccessful methos are called ONCE. This can be done as following.
Note that we have first imported loginRequest and isLoginSuccessful methods and then replaced with mock implementations so that they can be syped upon and asserted! When we have mock functions, we do not need to spy upon them explicitly! More on mocking can be read here https://vitest.dev/guide/mocking.html. Note also the use of ```async``` and ```await``` pattern!

```
  import { loginRequest } from "../../utils/httputils";
  import { isLoginSuccessful } from "../../utils/loginutils";
  ...

  it('test that loginRequest and isLoginSuccessful methods are called on handleLoginClick method call', async () => {
    vi.mock('../../utils/httputils.js');
    loginRequest = vi.fn().mockImplementation();

    vi.mock('../../utils/loginutils.js');
    isLoginSuccessful = vi.fn().mockImplementation(false);

    const wrapper = mount(LoginView);
    await wrapper.vm.handleLoginClick();

    expect(loginRequest).toHaveBeenCalledTimes(1);
    expect(isLoginSuccessful).toHaveBeenCalledTimes(1);
  })
```


As mentioned earlier, unit tests are about isolated parts or units. So, ideally we do not want to write unit tests that cover navigation between screens or views or that include interactions between components or systems. But one can write unit tests where the component/UI updates but still stays on the the same component. In this case, the underlying functions can be mocked as before.

A concrete usecase is as following. Lets us mock ```isLoginSuccessful``` method in ```LoginView.vue``` such that it returns false - meaning that login is unsuccessful. And then test that the LoginView should display a label ```Login failed!``` when ```handleLoginClick``` method is called (remember one step rule)!

```
  it('test that LoginView displays Login failed! when login is unsuccessful', async () => {
    vi.mock('../../utils/httputils.js');
    loginRequest = vi.fn().mockImplementation();

    vi.mock('../../utils/loginutils.js');
    isLoginSuccessful = vi.fn().mockImplementation(false);

    const wrapper = mount(LoginView);
    await wrapper.vm.handleLoginClick();

    const statusId = wrapper.find('#loginstatusLabel');
    expect(statusId.element.textContent).toBe('Login failed!');
  })
```

## Integration or e2e testing
In integration/e2e testing, individual components or pages or services or systems are combined together and tested as a group. Usually an integration/e2e testing is used to test 
- an eitire product
- a feature that requires interaction between more than one components/systems or multiple parts of an component or multiple systems
- a feature that flows through or validate many layers of an application
- a component or a system together with its dependencies
- an application by navigating through entire pages, for example in a real browser
- end users experience

In summary, e2e/integration tests ensure the correctness of the application and provide faster feedback loops. Read more on e2e tests here https://vuejs.org/guide/scaling-up/testing.html#e2e-testing

## e2e testing using Playwright
If you have not enabled 'Playwright' for e2e testing in your project while the project is created, never mind, you can do it again. Check out installation instructions here https://playwright.dev/docs/intro. Basically all you need to do is run this command ```npm init playwright@latest``` from your project directory. This will generate bunch of playwright related configuration files and example tests. You can also specify the playwright test directory during this process. I recommend to use ```playwrighttests```. This directory need to be excluded from the unit testing - for example in ```vitest.config.ts``` file.

The playwright tests can be run using a command ```npx playwright test```. The test reports can be seen using a command ```npx playwright show-report```. Note also that the tests can also be run in a UI using a command  ```npx playwright test --ui ```.

Note that the above command require that your application is up and running! Using a tool called ```start-server-and-test```, an application be started and tested using a single alias command in package.json file, for example by adding following command to the script section in package.json file.

```
"playwright": "start-server-and-test 'vite dev --port 4173' http://localhost:4173 'playwright test --ui'"
```

And then running a command ```npm run playwright```.


### Writing test for LoginView using playwright

Create a file ```login.spec.ts``` under your playwright test configuration directory!

First, lets start with simple test that tests that LoginView has a correct title text displayed - 'You did it!'

```
import { test, expect } from '@playwright/test';

test('has main title', async ({ page }) => {
  await page.goto('http://localhost:4173/');
  await expect(page.getByText('You did it!')).toBeVisible();
});
```

Run ```npm run playwright``` and verify that test passes. You can test that the test fails by changing the text to something else that does not exist in the LoginView page.

Lext, we can try to write a bit more complicated test scenario - for example

- user visits the page ( feks. our app running locally - http://localhost:4173/)
- the enter username
- enters a password
- clicks 'Sign in' button
- if username and password are valid, user successfully logs on 


Note that, it is possible to write such test manually. Playwright also offers very useful tool to generate tests by recording the user actions on screen. One variant of such is below. Note that before running this test, make sure that your backend (for LoginView) is also up and running (for example with ``` npm run mockapi```).

```
test('has fill in username and password and check status', async ({ page }) => {
  await page.goto('http://localhost:4173/');
  await page.locator('div').filter({ hasText: /^Username:$/ }).locator('#username').click();
  await page.locator('div').filter({ hasText: /^Username:$/ }).locator('#username').fill('Surya');
  await page.locator('div').filter({ hasText: /^Password: Sign in$/ }).locator('#password').click();
  await page.locator('div').filter({ hasText: /^Password: Sign in$/ }).locator('#password').fill('anthing');
  await page.getByRole('button', { name: 'Sign in' }).click();
  await expect(page.getByRole('heading', { name: 'Documentation' })).toBeVisible();
});
```

Run ```npm run playwright``` and verify that the test passes.


## e2e testing using Cypress
In this section we will use Cypress (https://www.cypress.io/), one of the most frequestly used tool for e2e of web applications. Note that Cypress can also be used for component testing. In this section we will only use Cypress for e2e testing!

Note that if you have selected cypress as test tool while you create your project, the Cypress configuration is already setup for your project. Inspect and verify that you have cypress folder in your roor project folder. And that you have script aliases ```test:e2e``` and ```test:e2e:dev``` are added to the scripts section in ```package.json``` file.

(If Cypress is not setup for your project, it recommended to either create a Vue project from scratch or follow the instructions here https://docs.cypress.io/guides/overview/why-cypress)

### Running existing e2e tests
Assuming the project is configured with Cypress on project creation, you can start/run Cypress integration tests using a command ```test:e2e:dev``` from the project root folder.
Then the Cypress configutaion window/dashboard will popup and then click 'Continue' (first time only), then select 'Chrome', and click 'Start E2E Testing'. 
You will then be redirected to a regular browser window that allows you to create new tests or select and run existing tests.

Note that when project is created first time, there is one test created by default - ```example.cy.ts``` under cypress/e2e folder in the project root folder. Its content is shown below. It first opens the applications root url and then looks for or asserts that loaded page contains the text "You did it!" under a ```<h1>``` tag. This is actually a part of ```HelloWorld``` component which is again a part of ```App.js``` component - the main component for the application.

```
// contents of example.cy.ts
describe("My First Test", () => {
  it("visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "You did it!");
  });
});
```

### Writing more e2e testing using Cypress
Follow the introductory guidelines here on how to write e2e tests using Cypress https://docs.cypress.io/guides/end-to-end-testing/writing-your-first-end-to-end-test#Write-your-first-test.

Writing an e2e test usually involves 3 steps
- Setup an application state, for example visit a page (or application)
- Take an action, for example interact with an element such as clicking a button. This step usually results an update in application state! Note also that this step is optional!
- Make an assertion about the resulting application state

Ok, lets now create a new e2e test, for a successful login scenario with the following steps

- Visit the application
- Test that it has elements for entering username and passwords, for a Sign in button
- Upon entering a username, password, and clicking signin button, test that a home screen is shown! Home screen for example, contains a 'Home' menu.

Note that we need to make sure that our mock API server is running and that its returns ```{ "status": "true" }``` on the endpoint ```http://localhost:3000/login```. Note that you might need to restart cypress tests with ```test:e2e:dev``` after mock server is started!

Ok, assuming that mocking server is up and running and returing response as mentioned above, lets write a Cypress integration test for a successfull login scenario!

- Create empty spec

  Click on the ```Specs``` menu icon on the left side of Cypress test dashboard, and then click on ```New spec``` button located on the top rigth of the same window. Choose ```Create new empty spec``` and give a proper name - for example ```cypress/e2e/test_login_success.cy.ts```. It will create an empty-ish skeleton like below. Now you can close the test creation dialog and open the created file in your favorite editor - for example IntelliJ or VSCode or something else!

  ```
  describe('empty spec', () => {
    it('passes', () => {
      cy.visit('https://example.cypress.io')
    })
  })
  ```

- Update the create spec
  Change the description from 'empty spec' to 'login success spec' and change 'cy.visit' to the root path '/'. Now if you go back to the Cypress test dialog, you will see the selecting the 'login success spec' will display your application!

  ```
  describe('login success spec', () => {
    it('passes', () => {
      cy.visit('/')
    })
  })
  ```

  Then update the spec as below. In the setup stage, we browse the login page. In the take action state, we enter username and password, and then click the sign in button. In the assert stage, we check that new page is displayed with a heading having text 'Documentation'.

  ```
  describe('login success spec', () => {
  it('test login success', () => {
      // setup
      cy.visit('/')
      // take actions
      cy.get('#username').type('myuser')
      cy.get('#password').type('mypassword')
      cy.get('#signinbutton').click()
      // assert
      cy.contains("h3", "Documentation");
    })
  })
  ```

  Thats it. Its time to run the newly written test. This can be done either headed or in a headless method. 

  The headed method is using a browser as before, i.e by running the command ```test:e2e:dev```.

  The headless method does not open a browser but tests are run hidden within a console. This method is especially useful in automation setting where tests can be run automatically as a part of build/deploy pipeline. 
  
  In order to headlessly run Cypress tests from a commandline, add the followling line under scripts section in a package.json file. And then execute a command ```test:e2e:headless```. Thats it, your tests will run headlessly inside a terminal or command console.

  ```
      "test:e2e:headless": "start-server-and-test 'vite dev --port 4173' :4173 'cypress run --e2e --headless'",
  ```

### Tips for writing Cypress e2e tests
While creating Cypress e2e tests we have chosen ```Create new empty spec``` option in the previous section. However, if you choose another available option 'Scaffold example specs', it will generate several example test specs and these will guide you (as referense examples) on how to write tests for various use cases. Some of the example specs generated are
- writing tests for actions, for example typing a text, focus a component, click a button, submitting a form, scrolling a view, etc
- handling cookies
- handling navigations
- handling network calls
- handling local storages
- and many more