import { test, expect } from "@playwright/test";

test("has main title", async ({ page }) => {
  await page.goto("http://localhost:4173/");

  // Expect a title "to contain" a substring.
  await expect(page.getByText("You did it!")).toBeVisible();
  //await expect(page).toHaveTitle(/You did it!/);
});

test("has login title", async ({ page }) => {
  await page.goto("http://localhost:4173/");

  // Expect a title "to contain" a substring.
  await expect(page.getByText("Please login")).toBeVisible();
  //await expect(page).toHaveTitle(/You did it!/);
});

test("has fill in username and password and check status", async ({ page }) => {
  await page.goto("http://localhost:4173/");
  await page
    .locator("div")
    .filter({ hasText: /^Username:$/ })
    .locator("#username")
    .click();
  await page
    .locator("div")
    .filter({ hasText: /^Username:$/ })
    .locator("#username")
    .fill("Surya");
  await page
    .locator("div")
    .filter({ hasText: /^Password: Sign in$/ })
    .locator("#password")
    .click();
  await page
    .locator("div")
    .filter({ hasText: /^Password: Sign in$/ })
    .locator("#password")
    .fill("anthing");
  await page.getByRole("button", { name: "Sign in" }).click();
  await expect(
    page.getByRole("heading", { name: "Documentation" }),
  ).toBeVisible();
});
